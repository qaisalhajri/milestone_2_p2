/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SOFT_TIMERS_H    /* Guard against multiple inclusion */
#define _SOFT_TIMERS_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "FreeRTOS.h"
#include "timers.h"
#include "debug.h"
#include "queues.h"
#include <string.h>

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
/* Define the Timers used in the project */
    //////////////////////////////////////////////////////////////////////////
    // Sensor Array Timer
    //////////////////////////////////////////////////////////////////////////
    TimerHandle_t adc_timer;
    EVENT_TYPE create_ADCTimer(unsigned int period);
    EVENT_TYPE start_ADCTimer();
    //////////////////////////////////////////////////////////////////////////
    // Wheel Motor Timer
    //////////////////////////////////////////////////////////////////////////
    TimerHandle_t game_timer;
    EVENT_TYPE create_gameTimer(unsigned int period);
    EVENT_TYPE start_gameTimer();
    EVENT_TYPE reset_gameTimer();
    //////////////////////////////////////////////////////////////////////////
    // Internal Callback Function
    //////////////////////////////////////////////////////////////////////////
    static void readADCValues(TimerHandle_t timer);
    static void gameOverFunction(TimerHandle_t timer);
    bool isGoal = false;
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
