/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _QUEUE_H    /* Guard against multiple inclusion */
#define _QUEUE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "FreeRTOS.h"
#include "queue.h"
#include "debug.h"
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    

#define INSTRUCTION_MSG_TYPE            unsigned char[50]
#define GAME_STATE_MSG_TYPE             unsigned int
#define RPI_OUT_MSG_TYPE                unsigned char[50]
#define MSG_QUEUE_LENGTH    10
    
    // Instantiate all the queue handles and their names
    ///////////////////////////////////////////////////////////////////////////
    // Motor Related Queue
    //////////////////////////////////////////////////////////////////////////
    QueueHandle_t instruction_in;
    /* Functions related to the motor message queue */
    EVENT_TYPE create_instructionInQueue();
    EVENT_TYPE sendTo_instructionInQueue(void* message);
    EVENT_TYPE ISRSendTo_instructionInQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    EVENT_TYPE receiveFrom_instructionInQueue(void* message);
    ///////////////////////////////////////////////////////////////////////////
    // UART Related Message Queues
    //////////////////////////////////////////////////////////////////////////
    QueueHandle_t game_state;
    /* Functions related to the status queue */
    EVENT_TYPE create_gameStateQueue();
    EVENT_TYPE sendTo_gameStateQueue(void* message);
    EVENT_TYPE receiveFrom_gameStateQueue(void* message);
    EVENT_TYPE ISRSendTo_gameStateQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    /* TX Queue */
    QueueHandle_t status;
    EVENT_TYPE create_statusQueue();
    EVENT_TYPE sendTo_statusQueue(void *message);
    EVENT_TYPE receiveFrom_statusQueue(void *message);
   /* Functions related to the rpi out queue */
    QueueHandle_t rpi_out;
    EVENT_TYPE create_rpiOutQueue();
    EVENT_TYPE sendTo_rpiOutQueue(void *message);
    EVENT_TYPE ISR_receiveFrom_rpiOutQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    bool isrpiOutQueueEmpty();
    ///////////////////////////////////////////////////////////////////////////
    // Stuff
    ///////////////////////////////////////////////////////////////////////////
    /* Functions related to the UART */
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
