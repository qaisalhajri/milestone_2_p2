#include "queues.h"
///////////////////////////////////////////////////////////////////////////
// Game State Related Queue
//////////////////////////////////////////////////////////////////////////

EVENT_TYPE create_gameStateQueue() {
    game_state = xQueueCreate(MSG_QUEUE_LENGTH, sizeof (GAME_STATE_MSG_TYPE));
    if (game_state == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE sendTo_gameStateQueue(void* message) {
    if (xQueueSendToBack(game_state, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE receiveFrom_gameStateQueue(void* message) {
    if (xQueueReceive(game_state, message, portMAX_DELAY) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE ISRSendTo_gameStateQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if (xQueueSendToBackFromISR(game_state, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////
// Instruction Input Queue
//////////////////////////////////////////////////////////////////////////

EVENT_TYPE create_instructionInQueue() {
    instruction_in = xQueueCreate(MSG_QUEUE_LENGTH, sizeof (INSTRUCTION_MSG_TYPE));
    if (instruction_in == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE sendTo_instructionInQueue(void* message) {
    if (xQueueSendToBack(instruction_in, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE ISRSendTo_instructionInQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if (xQueueSendToBackFromISR(instruction_in, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE receiveFrom_instructionInQueue(void* message) {
    if (xQueueReceive(instruction_in, message, portMAX_DELAY) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////
// Status Related Queue
//////////////////////////////////////////////////////////////////////////

EVENT_TYPE create_statusQueue() {
    status = xQueueCreate(MSG_QUEUE_LENGTH, sizeof (INSTRUCTION_MSG_TYPE));
    if (status == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE sendTo_statusQueue(void *message) {
    if (xQueueSendToBack(status, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE receiveFrom_statusQueue(void *message) {
    if (xQueueReceive(status, message, portMAX_DELAY) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////
// Wifly Out Related Queue
//////////////////////////////////////////////////////////////////////////

EVENT_TYPE create_rpiOutQueue() {
    rpi_out = xQueueCreate(MSG_QUEUE_LENGTH, sizeof (RPI_OUT_MSG_TYPE));
    if (rpi_out == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE sendTo_rpiOutQueue(void *message) {
    if (xQueueSendToBack(rpi_out, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE ISR_receiveFrom_rpiOutQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if (xQueueReceiveFromISR(rpi_out, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

bool isWiflyOutQueueEmpty() {
    if (xQueueIsQueueEmptyFromISR(rpi_out) == pdTRUE) {
        return true;
    }
    return false;
}
////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////
