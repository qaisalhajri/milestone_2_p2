#include "software_timers.h"
////////////////////////////////////////////////////////
// Create ADC Timer
////////////////////////////////////////////////////////
    EVENT_TYPE create_ADCTimer(unsigned int period) {
        adc_timer = xTimerCreate("ADC Timer", 
                pdMS_TO_TICKS(period),
                pdTRUE,
                0,
                readADCValues);
        if(adc_timer == NULL) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    ////////////////////////////////////////////////////////////
    // Start ADC Timer
    ////////////////////////////////////////////////////////////
    EVENT_TYPE start_ADCTimer() {
        if(xTimerStart(adc_timer, 0) != pdPASS) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    //////////////////////////////////////////////////////////////
    // Create Game Over Timer
    //////////////////////////////////////////////////////////////
    EVENT_TYPE create_gameTimer(unsigned int period) {
        game_timer = xTimerCreate("Game Timer",
                pdMS_TO_TICKS(period),
                pdFALSE,
                0,
                gameOverFunction);
        if(game_timer == NULL) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    //////////////////////////////////////////////////////////////
    // Start Game Over Timer
    //////////////////////////////////////////////////////////////
    EVENT_TYPE start_gameTimer() {
        if(xTimerStart(game_timer, 0) != pdPASS) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    //////////////////////////////////////////////////////////////
    // Reset Game Over Timer
    //////////////////////////////////////////////////////////////
    EVENT_TYPE reset_gameTimer() {
        if(xTimerReset(game_timer, 0) != pdPASS) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    //////////////////////////////////////////////////////////////
    // Internal Functions
    //////////////////////////////////////////////////////////////
    static void readADCValues(TimerHandle_t timer) {
        PORTA = PORTA ^ 0b01000;
        PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_3); /*Toggle the LED Every period */
        DRV_ADC_Start();
        unsigned int distance;
        if(DRV_ADC_SamplesAvailable(0)) {
           distance = DRV_ADC_SamplesRead(0); 
        } else {
            distance = 0;
        }
        if(distance >= 950) {
            unsigned int goal = 200;
            isGoal = true;
            dbgOutputEvent(ISRSendTo_gameStateQueue(&goal, pdFALSE));
        }
    }
    static void gameOverFunction(TimerHandle_t timer) {
        if(isGoal == false) {
           unsigned int gameOver = 100;
            dbgOutputEvent(ISRSendTo_gameStateQueue(&gameOver, pdFALSE)); 
        }
        
    }