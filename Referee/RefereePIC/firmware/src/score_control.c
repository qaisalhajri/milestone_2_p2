/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    score_control.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "score_control.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

SCORE_CONTROL_DATA score_controlData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SCORE_CONTROL_Initialize ( void )

  Remarks:
    See prototype in score_control.h.
 */

void SCORE_CONTROL_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    score_controlData.state = SCORE_CONTROL_STATE_INIT;

    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
    create_gameTimer(5000); /* 5 Second Timer */ 
    dbgOutputEvent(create_gameStateQueue());
    dbgOutputEvent(SCORE_CONTROL_INIT_SUCCESS);
}


/******************************************************************************
  Function:
    void SCORE_CONTROL_Tasks ( void )

  Remarks:
    See prototype in score_control.h.
 */

void SCORE_CONTROL_Tasks ( void )
{
    dbgOutputEvent(SCORE_CONTROL_BEGIN);
    unsigned int data;
    
    dbgOutputEvent(receiveFrom_gameStateQueue(&data)); 
    if(data == 4) {
        game = KICK;
    } 
    dbgOutputEvent(RECEIVED_FROM_QUEUE);
    /* Receive Data and Do something with it */
    switch(game) {
        case START: {
           break; 
        }
        case STOP: {
            break;
        }
        case RESET: {
            break;
        }
        case KICK: {
            dbgOutputEvent(reset_gameTimer());
            game = OBSERVE;
            break;
        }
        case OBSERVE: {
            if(data == 100) {
                dbgOutputEvent(sendTo_statusQueue(&gameOVER[0]));
                dbgOutputEvent(SENT_TO_QUEUE);
            } else {
                if(data == 200) {
                    dbgOutputEvent(sendTo_statusQueue(&GOAL[0]));
                    dbgOutputEvent(SENT_TO_QUEUE);
                }
            }
            break;
        }
    }
    dbgOutputEvent(SCORE_CONTROL_LEAVE);
}

 

/*******************************************************************************
 End of File
 */
