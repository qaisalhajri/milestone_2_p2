#include "debug.h"

void valueGPIODisplay(unsigned char outVal) {
    /* Pin 46 - Corresponds to bit 0*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_1, (outVal & 0x01));
    /* Pin 47 - Corresponds to bit 1*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_6, (outVal & 0x02) >> 1);
    /* Pin 48 - Corresponds to bit 2*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_8, (outVal & 0x04) >> 2);
    /* Pin 49 - Corresponds to bit 3*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_11, (outVal & 0x08) >> 3);
    /* Pin 50 - Corresponds to bit 4*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_7, (outVal & 0x10) >> 4);
    /* Pin 51 - Corresponds to bit 5*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_8, (outVal & 0x20) >> 5);
    /* Pin 52 - Corresponds to bit 6*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_6, (outVal & 0x40) >> 6);
    /* Pin 53 - Corresponds to bit 7 */
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_9, (outVal & 0x80) >> 7);

}

/*****************************************************************************
 * 
 *****************************************************************************/
void EventGPIODisplay(unsigned char outVal) {

    PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_E, outVal);
}

/*****************************************************************************
 * 
 *****************************************************************************/
void dbgOutputEvent(unsigned char outVal) {
    // outVal corresponds to an event
    // outVal is a unique value for a specific event
    if (outVal == EVENT_FAIL) {
        stopEverything();
    }
    if (outVal <= 127) {
        // Output to a different 8 I/O pins
        // Pins need to be accessible when used with other boards
        EventGPIODisplay(outVal);
    }
}

/*****************************************************************************
 * 
 *****************************************************************************/
void dbgOutputVal(unsigned int outVal) {
    // outVal corresponds to an event
    // outVal is a unique value for a specific event

    // Output to a different 8 I/O pins
    // Pins need to be accessible when used with other boards
    valueGPIODisplay((outVal & 0xFF00) >> 8);
    valueGPIODisplay(outVal);
}

/*****************************************************************************
 * 
 *****************************************************************************/
void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter) {
    // Output the average of every four sensor messages
    while (PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, outVal);
    while (PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, centi);
    while (PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, meter);

}

/*****************************************************************************
 * 
 *****************************************************************************/
void stopEverything() {
    // Stop everything from running and end the program
    /************************************************************************
     * Disable Interrupts
     ************************************************************************/
    SYS_INT_StatusGetAndDisable();
    //DRV_ADC_Stop();

    /************************************************************************
     * Disable Suspend All Threads
     ************************************************************************/
    while (1) {
        //DBG_EVENT_OUT(EVENT_FAIL);
        EventGPIODisplay(EVENT_FAIL);
        vTaskSuspend(NULL);
    }

    // Enter infinite while loop 
    // Then send out that the event failed

}

void outputStringGPIO(unsigned char* msg, int length) {
    int i = 0;
    while (i < length) {
        valueGPIODisplay(msg[i]);
        i++;
    }
}