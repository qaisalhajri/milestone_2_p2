/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    DEBUG.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _DEBUG_H    /* Guard against multiple inclusion */
#define _DEBUG_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "peripheral/ports/plib_ports.h"
#include "driver/usart/drv_usart.h"



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
#define EVENT_TYPE                  unsigned char
#define EVENT_FAIL                  0xFF
#define EVENT_SUCCESS               0x1
    /*****************************************************************************
     * TASK EVENT NUMBERS
     *****************************************************************************/
#define MOTOR_CONTROL_INIT_SUCCESS      0x10
#define MOTOR_CONTROL_BEGIN             0x1A
#define MOTOR_CONTROL_LEAVE             0x1F
#define PIXY_IN_INIT_SUCCESS            0x20
#define PIXY_IN_BEGIN                   0x2A
#define PIXY_IN_LEAVE                   0x2F
#define WIFLY_IN_INIT_SUCCESS           0x30
#define WIFLY_IN_BEGIN                  0x3A
#define WIFLY_IN_LEAVE                  0x3F
#define WIFLY_OUT_INIT_SUCCESS          0x40
#define WIFLY_OUT_BEGIN                 0x4A
#define WIFLY_OUT_LEAVE                 0x4F
    /*****************************************************************************
     * ISR EVENT NUMBERS
     *****************************************************************************/
#define WIFLY_RECEIVE_ISR               0x70
#define WIFLY_TRANSMIT_ISR              0x71
#define WIFLY_ERROR_ISR                 0x72
#define LEAVING_UART_ISR                0x7A
    /*****************************************************************************
        * SHARED EVENTS - QUEUES
     *****************************************************************************/
#define QUEUE_CREATE_SUCCESS            0x6A
#define SENT_TO_QUEUE                   0x60
#define RECEIVED_FROM_QUEUE             0x61
    /*****************************************************************************
        * SHARED EVENTS - TIMER
     *****************************************************************************/
#define TIMER_CREATE_SUCCESS            0x5A
#define TIMER_CALL_BACK_ENTER           0x50
#define TIMER_CALL_BACK_LEAVE           0x51

    void dbgOutputVal(unsigned int outVal);
    /***************************************************************************
     * Debug UART Value
     * Value Written to UART
     * Verify that the value of outputVal is less than or equal to 127
     ***************************************************************************/
    void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter);
    /***************************************************************************
     * STOP EVERYTHING ROUTINE
     * This routine makes sure everything stops working 
     ***************************************************************************/
    void stopEverything();
    void dbgOutputEvent(unsigned char outVal);
    ////////////////////////////////////////////////////////////////////////////
    // Internal Functions
    ////////////////////////////////////////////////////////////////////////////
    void eventGPIODisplay(unsigned char outVal);
    void valueGPIODispay(unsigned char outVal);
    void outputStringGPIO(unsigned char* msg, int length);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
