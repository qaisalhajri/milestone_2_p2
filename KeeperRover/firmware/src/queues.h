/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _QUEUE_H    /* Guard against multiple inclusion */
#define _QUEUE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "FreeRTOS.h"
#include "queue.h"
#include "debug.h"
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct {
        
    };
#define UART_MSG_TYPE            unsigned char[50]
#define MOTOR_MSG_TYPE           unsigned int[2]
#define STATUS_MSG_TYPE          unsigned int[2]
#define MSG_QUEUE_LENGTH    10
    
    // Instantiate all the queue handles and their names
    /////////////////////////////////////wwwwww//////////////////////////////////////
    // Motor Related Queue
    //////////////////////////////////////////////////////////////////////////
    QueueHandle_t motor_in;
    /* Functions related to the motor message queue */
    EVENT_TYPE create_motorInQueue();
    EVENT_TYPE sendTo_motorInQueue(void* message);
    EVENT_TYPE ISRSendTo_motorInQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    EVENT_TYPE receiveFrom_motorInQueue(void* message);
    ///////////////////////////////////////////////////////////////////////////
    // UART Related Message Queues
    //////////////////////////////////////////////////////////////////////////
    QueueHandle_t status;
    /* Functions related to the status queue */
    EVENT_TYPE create_statusQueue();
    EVENT_TYPE sendTo_statusQueue(void* message);
    EVENT_TYPE receiveFrom_statusQueue(void* message);
   /* Functions related to the wifly out queue */
    QueueHandle_t wifly_out;
    EVENT_TYPE create_wiflyOutQueue();
    EVENT_TYPE sendTo_wiflyOutQueue(void *message);
    EVENT_TYPE ISR_receiveFrom_wiflyOutQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    bool isWiflyOutQueueEmpty();
    /* Functions related to Wifly_in*/
    QueueHandle_t wifly_in;
    EVENT_TYPE create_wiflyInQueue();
    EVENT_TYPE ISRSendTo_wiFlyInQueue(void *message, portBASE_TYPE* pxHigherPriorityTaskWoken);
    EVENT_TYPE receiveFrom_wiflyInQueue(void* message);
    ///////////////////////////////////////////////////////////////////////////
    // Stuff
    ///////////////////////////////////////////////////////////////////////////
    /* Functions related to the UART */
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
