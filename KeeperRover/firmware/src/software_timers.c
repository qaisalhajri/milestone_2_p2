#include "software_timers.h"
//////////////////////////////////////////////////////////////////////////////
// Create Sensor Array Timer
//////////////////////////////////////////////////////////////////////////////
    EVENT_TYPE create_sensorArrayTimer(unsigned int period) {
        sensor_array_timer = xTimerCreate("Sensor Array Timer",
                pdMS_TO_TICKS(period),
                pdTRUE,                 /*Auto reset timer */ 
                0,
                readSensorArrayValues);
        if(sensor_array_timer == NULL) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
//////////////////////////////////////////////////////////////////////////////
// Start Sensor Array Timer
//////////////////////////////////////////////////////////////////////////////
    EVENT_TYPE start_sensorArrayTimer() {
        if(xTimerStart(sensor_array_timer, 0) != pdPASS) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
///////////////////////////////////////////////////////////////////////////////
// Create Wheel Motor Timer
///////////////////////////////////////////////////////////////////////////////
    EVENT_TYPE create_wheelMotorTimer(unsigned int period) {
        wheel_motor_timer = xTimerCreate("Wheel Array Timer", 
                pdMS_TO_TICKS(period),
                pdTRUE,
                0,
                readWheelEncoderValues);
        if(wheel_motor_timer == NULL) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
//////////////////////////////////////////////////////////////////////////////
// Start Wheel Array Timer
//////////////////////////////////////////////////////////////////////////////
    EVENT_TYPE start_wheelMotorTimer() {
        if(xTimerStart(wheel_motor_timer, 0) != pdPASS) {
            return EVENT_FAIL;
        }
        return EVENT_SUCCESS;
    }
    
//////////////////////////////////////////////////////////////////////////////
// Sensor Value Readings
/////////////////////////////////////////////////////////////////////////////
    static void readSensorArrayValues(TimerHandle_t timer) {
        PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A, PORTS_BIT_POS_3);
        dbgOutputEvent(TIMER_CALL_BACK_ENTER);
        dbgOutputEvent(TIMER_CALL_BACK_LEAVE);
    }
//////////////////////////////////////////////////////////////////////////////
// Encoder Value Readings
/////////////////////////////////////////////////////////////////////////////
    static void readWheelEncoderValues(TimerHandle_t timer) {
        dbgOutputEvent(TIMER_CALL_BACK_ENTER);
        TickType_t curTime;
        curTime = xTaskGetTickCount();
        LeftMotor_EncoderValue  = DRV_TMR1_CounterValueGet(); /* Timer 1's counter value is driven by rising edge of SA1 Encoder Ticks */
        RightMotor_EncoderValue = DRV_TMR2_CounterValueGet(); /* Timer 2's counter value is driven by rising edge of SA1 Encoder Ticks */
        // The value found in the timer divided by 4 is a good approximation of
        // the speed in mm/s
        motor_speed_left = LeftMotor_EncoderValue >> 2;
        motor_speed_right = RightMotor_EncoderValue >> 2;
        motor_speed[0] = motor_speed_left;
        motor_speed[1] = motor_speed_right;
        EVENT_TYPE event;
        event = ISRSendTo_motorInQueue(&motor_speed[0], pdFALSE);
        dbgOutputEvent(SENT_TO_QUEUE);
        DRV_TMR1_CounterClear();
        DRV_TMR2_CounterClear();
        dbgOutputEvent(TIMER_CALL_BACK_LEAVE);
    }