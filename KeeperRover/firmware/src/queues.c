#include "queues.h"

///////////////////////////////////////////////////////////////////////////
// Motor Related Queue
//////////////////////////////////////////////////////////////////////////
EVENT_TYPE create_motorInQueue() {
    motor_in = xQueueCreate(MSG_QUEUE_LENGTH, sizeof(MOTOR_MSG_TYPE));
    if(motor_in == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE sendTo_motorInQueue(void* message) {
    if(xQueueSendToBack(motor_in, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE ISRSendTo_motorInQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if(xQueueSendToBackFromISR(motor_in, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

EVENT_TYPE receiveFrom_motorInQueue(void* message) {
    if(xQueueReceive(motor_in, message, portMAX_DELAY) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////
// Status Related Queue
//////////////////////////////////////////////////////////////////////////
EVENT_TYPE create_statusQueue() {
    status = xQueueCreate(MSG_QUEUE_LENGTH, sizeof(STATUS_MSG_TYPE));
    if(status == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE sendTo_statusQueue(void* message) {
    if(xQueueSendToBack(status, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE receiveFrom_statusQueue(void* message) {
    if(xQueueReceive(status, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////
// Wifly Out Related Queue
//////////////////////////////////////////////////////////////////////////

EVENT_TYPE create_wiflyOutQueue() {
    wifly_out = xQueueCreate(MSG_QUEUE_LENGTH, sizeof(UART_MSG_TYPE));
    if(wifly_out == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE sendTo_wiflyOutQueue(void *message) {
    if(xQueueSendToBack(wifly_out, message, (TickType_t) 0) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE ISR_receiveFrom_wiflyOutQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    if(xQueueReceiveFromISR(wifly_out, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
bool isWiflyOutQueueEmpty() {
    if(xQueueIsQueueEmptyFromISR(wifly_out) == pdTRUE) {
        return true;
    }
    return false;
}
////////////////////////////////////////////////////////////////////////////////
EVENT_TYPE create_wiflyInQueue() {
    wifly_in = xQueueCreate(MSG_QUEUE_LENGTH, sizeof(UART_MSG_TYPE));
    if(wifly_in == NULL) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE ISRSendTo_wiFlyInQueue(void *message, portBASE_TYPE* pxHigherPriorityTaskWoken){
    if(xQueueSendToBackFromISR(wifly_in, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
EVENT_TYPE receiveFrom_wiflyInQueue(void* message) {
    if(xQueueReceive(wifly_in, message, portMAX_DELAY) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
    //////////////////////////////////////////////////////
