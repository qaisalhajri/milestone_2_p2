/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SOFT_TIMERS_H    /* Guard against multiple inclusion */
#define _SOFT_TIMERS_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "FreeRTOS.h"
#include "timers.h"
#include "debug.h"
#include "queues.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
/* Define the Timers used in the project */
    //////////////////////////////////////////////////////////////////////////
    // Sensor Array Timer
    //////////////////////////////////////////////////////////////////////////
    TimerHandle_t sensor_array_timer;
    EVENT_TYPE create_sensorArrayTimer(unsigned int period);
    EVENT_TYPE start_sensorArrayTimer();
    //////////////////////////////////////////////////////////////////////////
    // Wheel Motor Timer
    //////////////////////////////////////////////////////////////////////////
    TimerHandle_t wheel_motor_timer;
    EVENT_TYPE create_wheelMotorTimer(unsigned int period);
    EVENT_TYPE start_wheelMotorTimer();
    
    //////////////////////////////////////////////////////////////////////////
    // Internal Callback Function
    //////////////////////////////////////////////////////////////////////////
    unsigned int LeftMotor_EncoderValue, RightMotor_EncoderValue, 
                 motor_speed_left, motor_speed_right;
    unsigned int motor_speed[2];
    static void readWheelEncoderValues(TimerHandle_t timer);
    static void readSensorArrayValues(TimerHandle_t timer);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
