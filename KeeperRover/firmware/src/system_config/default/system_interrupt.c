/*******************************************************************************
 System Interrupts File

  File Name:
    system_interrupt.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.

  Summary:
    This file contains source code for the interrupt vector functions in the
    system.

  Description:
    This file contains source code for the interrupt vector functions in the
    system.  It implements the system and part specific vector "stub" functions
    from which the individual "Tasks" functions are called for any modules
    executing interrupt-driven in the MPLAB Harmony system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    interrupt-driven in the system.  These handles are passed into the individual
    module "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system/common/sys_common.h"
#include "sensor.h"
#include "pixy_input.h"
#include "motor_control.h"
#include "wifly_tx.h"
#include "wifly_rx.h"
#include "system_definitions.h"
// USART Receive Helper Variables
unsigned char wifly_input_msg[50];
unsigned int w_in = 0;
unsigned int length = 0;
// USART Transmit Helper variables
unsigned char wifly_out_msg[50];
unsigned int w_out = 0;

typedef enum {
    NOT_SENDING_MSG, SENDING_MSG
} MSG_STATE;

MSG_STATE transmitterState;
// *****************************************************************************
// *****************************************************************************
// Section: System Interrupt Vector Functions
// *****************************************************************************
// *****************************************************************************

void IntHandlerDrvUsartInstance0(void) {
    // Receive
    if (SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_RECEIVE)) {
        dbgOutputEvent(WIFLY_RECEIVE_ISR);
        if (PLIB_USART_ReceiverDataIsAvailable(USART_ID_1)) {
            if (w_in == 0) {
                length = PLIB_USART_ReceiverByteReceive(USART_ID_1);
            } else {
                wifly_input_msg[w_in] = PLIB_USART_ReceiverByteReceive(USART_ID_1);
                //length = wifly_input_msg[w_in]; /* Assume the first message received is the size of the message */
                w_in++;
            }
        }
        if (w_in >= length) {
            ISRSendTo_wiFlyInQueue(&wifly_input_msg[0], pdFALSE);
            dbgOutputEvent(SENT_TO_QUEUE);
            w_in = 0;
        }
        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_RECEIVE);
    }
    // Transmit
    if (SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_TRANSMIT)) {
        dbgOutputEvent(WIFLY_TRANSMIT_ISR);
        switch (transmitterState) {
            case NOT_SENDING_MSG:
            {
                /* If we are in this state and the queue is still not empty, then we need to make it empty*/
                if (!isWiflyOutQueueEmpty()) {
                    dbgOutputEvent(ISR_receiveFrom_wiflyOutQueue(wifly_out_msg, pdFALSE));
                    dbgOutputEvent(RECEIVED_FROM_QUEUE);
                    w_out = 0;
                    transmitterState = SENDING_MSG;
                } else {
                    SYS_INT_SourceDisable(INT_SOURCE_USART_1_TRANSMIT);
                }
                break;
            }
            case SENDING_MSG:
            {
                if (!PLIB_USART_TransmitterBufferIsFull(USART_ID_1)) {
                    PLIB_USART_TransmitterByteSend(USART_ID_1, wifly_out_msg[w_out]);
                    w_out++;
                }
                if (w_out >= length) {
                    if (!isWiflyOutQueueEmpty()) {
                        dbgOutputEvent(ISR_receiveFrom_wiflyOutQueue(wifly_out_msg, pdFALSE));
                        dbgOutputEvent(RECEIVED_FROM_QUEUE);
                        w_out = 0;
                    } else {
                        transmitterState = NOT_SENDING_MSG;
                        SYS_INT_SourceDisable(INT_SOURCE_USART_1_TRANSMIT);
                        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_TRANSMIT);
                    }
                }
                break;
            }
            default:
                break;
        }
        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_TRANSMIT);
    }
    // Error
    if (SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_ERROR)) {
        dbgOutputEvent(0x00);
        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_ERROR);
    }
    dbgOutputEvent(LEAVING_UART_ISR);
}

void IntHandlerDrvUsartInstance1(void) {
    DRV_USART_TasksTransmit(sysObj.drvUsart1);
    DRV_USART_TasksError(sysObj.drvUsart1);
    DRV_USART_TasksReceive(sysObj.drvUsart1);
}

void IntHandlerDrvTmrInstance0(void) {
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_TIMER_2);
}

void IntHandlerDrvTmrInstance1(void) {
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_TIMER_3);
}

void IntHandlerDrvTmrInstance2(void) {
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_TIMER_4);
}

/*******************************************************************************
 End of File
 */
