/*******************************************************************************
 System Tasks File

  File Name:
    system_tasks.c

  Summary:
    This file contains source code necessary to maintain system's polled state
    machines.

  Description:
    This file contains source code necessary to maintain system's polled state
    machines.  It implements the "SYS_Tasks" function that calls the individual
    "Tasks" functions for all the MPLAB Harmony modules in the system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    polled in the system.  These handles are passed into the individual module
    "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "system_config.h"
#include "system_definitions.h"
#include "sensor.h"
#include "pixy_input.h"
#include "motor_control.h"
#include "wifly_tx.h"
#include "wifly_rx.h"


// *****************************************************************************
// *****************************************************************************
// Section: Local Prototypes
// *****************************************************************************
// *****************************************************************************


 
static void _SYS_Tasks ( void );
 
 
static void _SENSOR_Tasks(void);
static void _PIXY_INPUT_Tasks(void);
static void _MOTOR_CONTROL_Tasks(void);
static void _WIFLY_TX_Tasks(void);
static void _WIFLY_RX_Tasks(void);


// *****************************************************************************
// *****************************************************************************
// Section: System "Tasks" Routine
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void SYS_Tasks ( void )

  Remarks:
    See prototype in system/common/sys_module.h.
*/

void SYS_Tasks ( void )
{
    /* Create OS Thread for Sys Tasks. */
    xTaskCreate((TaskFunction_t) _SYS_Tasks,
                "Sys Tasks",
                1024, NULL, 0, NULL);

 
 
    /* Create OS Thread for SENSOR Tasks. */
    xTaskCreate((TaskFunction_t) _SENSOR_Tasks,
                "SENSOR Tasks",
                1024, NULL, 1, NULL);

    /* Create OS Thread for PIXY_INPUT Tasks. */
    xTaskCreate((TaskFunction_t) _PIXY_INPUT_Tasks,
                "PIXY_INPUT Tasks",
                1024, NULL, 1, NULL);

    /* Create OS Thread for MOTOR_CONTROL Tasks. */
    xTaskCreate((TaskFunction_t) _MOTOR_CONTROL_Tasks,
                "MOTOR_CONTROL Tasks",
                1024, NULL, 1, NULL);

    /* Create OS Thread for WIFLY_TX Tasks. */
    xTaskCreate((TaskFunction_t) _WIFLY_TX_Tasks,
                "WIFLY_TX Tasks",
                1024, NULL, 1, NULL);

    /* Create OS Thread for WIFLY_RX Tasks. */
    xTaskCreate((TaskFunction_t) _WIFLY_RX_Tasks,
                "WIFLY_RX Tasks",
                1024, NULL, 1, NULL);

    /**************
     * Start RTOS * 
     **************/
    vTaskStartScheduler(); /* This function never returns. */
}


/*******************************************************************************
  Function:
    void _SYS_Tasks ( void )

  Summary:
    Maintains state machines of system modules.
*/
static void _SYS_Tasks ( void)
{
    while(1)
    {
        /* Maintain system services */

        /* Maintain Device Drivers */
 
 

        /* Maintain Middleware */

        /* Task Delay */
    }
}

 
 

/*******************************************************************************
  Function:
    void _SENSOR_Tasks ( void )

  Summary:
    Maintains state machine of SENSOR.
*/

static void _SENSOR_Tasks(void)
{
    while(1)
    {
        SENSOR_Tasks();
    }
}


/*******************************************************************************
  Function:
    void _PIXY_INPUT_Tasks ( void )

  Summary:
    Maintains state machine of PIXY_INPUT.
*/

static void _PIXY_INPUT_Tasks(void)
{
    while(1)
    {
        PIXY_INPUT_Tasks();
    }
}


/*******************************************************************************
  Function:
    void _MOTOR_CONTROL_Tasks ( void )

  Summary:
    Maintains state machine of MOTOR_CONTROL.
*/

static void _MOTOR_CONTROL_Tasks(void)
{
    while(1)
    {
        MOTOR_CONTROL_Tasks();
    }
}


/*******************************************************************************
  Function:
    void _WIFLY_TX_Tasks ( void )

  Summary:
    Maintains state machine of WIFLY_TX.
*/

static void _WIFLY_TX_Tasks(void)
{
    while(1)
    {
        WIFLY_TX_Tasks();
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}


/*******************************************************************************
  Function:
    void _WIFLY_RX_Tasks ( void )

  Summary:
    Maintains state machine of WIFLY_RX.
*/

static void _WIFLY_RX_Tasks(void)
{
    while(1)
    {
        WIFLY_RX_Tasks();
    }
}


/*******************************************************************************
 End of File
 */
