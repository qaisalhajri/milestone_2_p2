/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    motor_control.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "motor_control.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
 */

MOTOR_CONTROL_DATA motor_controlData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
 */

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
 */


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void MOTOR_CONTROL_Initialize ( void )

  Remarks:
    See prototype in motor_control.h.
 */

void MOTOR_CONTROL_Initialize(void) {
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
    // Initialize motor_in queue
    dbgOutputEvent(create_motorInQueue());
    // Initialize motor timer
    dbgOutputEvent(create_wheelMotorTimer(125)); /* 125 ms make the timer send a signal */
    // Initialize the timers
    DRV_TMR0_Start();
    DRV_TMR1_Start();
    DRV_TMR2_Start();
    // Initialize the output compares
    DRV_OC0_Enable();
    DRV_OC1_Enable();
    DRV_OC0_Start(); // Corresponds to the left motor
    DRV_OC1_Start(); // Corresponds to the right motor
    // Initially the motor is 0
    DRV_OC0_PulseWidthSet(0x00);
    DRV_OC1_PulseWidthSet(0x00);
    dbgOutputEvent(start_wheelMotorTimer());
    dbgOutputEvent(MOTOR_CONTROL_INIT_SUCCESS);
}

/******************************************************************************
  Function:
    void MOTOR_CONTROL_Tasks ( void )

  Remarks:
    See prototype in motor_control.h.
 */

void MOTOR_CONTROL_Tasks(void) {
    // Adjusting is done in sensor thread
    // It sets the signal for adjustment
    // adjustLeft means we have gone too far left and must come back
    // adjustRight means we have gone too far right and must come back
    dbgOutputEvent(MOTOR_CONTROL_BEGIN);
    if (adjustLeft == true) {
        leftMotor_control(40, FORWARD);
        rightMotor_control(80, FORWARD);
    } else if (adjustRight == true) {
        leftMotor_control(80, FORWARD);
        rightMotor_control(60, FORWARD);
    } else {
        /* Receive MSG from MSG Queue */
        unsigned int motorCtrlMsg[2];
        dbgOutputEvent(receiveFrom_motorInQueue(&motorCtrlMsg[0]));
        dbgOutputEvent(RECEIVED_FROM_QUEUE);
        // count to 5 seconds and switch the direction of motion
        if(count5Sec >= 40) {
            switch(dir) {
                case FORWARD: {
                    leftMotor_control(50, BACKWARD);
                    rightMotor_control(60, BACKWARD);
                    dir = BACKWARD;
                }
                break;
                case BACKWARD: {
                    leftMotor_control(50, FORWARD);
                    rightMotor_control(60, FORWARD);
                    dir = FORWARD;
                }
                break;
                default :
                    break;
            }
        } else {
            count5Sec++;
        }
    }
    dbgOutputEvent(MOTOR_CONTROL_LEAVE);
}

////////////////////////////////////////////////////////////////////////////////
// Internal Functions Related to Motor Control
/////////////////////////////////////////////////////////////////////////////////

void leftMotor_control(unsigned int speed, DIRECTION dir) {
    pwmVal = speedConversion(speed);
    DRV_OC0_PulseWidthSet(pwmVal);
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_C, PORTS_BIT_POS_14, dir);
}

void rightMotor_control(unsigned int speed, DIRECTION dir) {
    pwmVal = speedConversion(speed);
    DRV_OC1_PulseWidthSet(pwmVal);
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_1, dir);
}

unsigned int speedConversion(unsigned int speed) {
    return 8 * ((156 * speed) + 25); /* Linear Relationship between 0 - 100 and 0 - 15625 */
}
/*******************************************************************************
 End of File
 */
